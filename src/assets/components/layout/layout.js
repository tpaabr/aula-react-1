import React from 'react';
import Footer from '../footer/footer'
import Header from '../header/header'
import BurgerMenu from '../burgerMenu/burgerMenu'

const Layout = (props) => {
    return(
        <React.Fragment>
            {props.children}
            <Footer/>
        </React.Fragment>
    )
}

export default Layout;