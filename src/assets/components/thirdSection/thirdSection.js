import React from 'react';
import './thirdSection.css';
import PrimaryButton from '../primaryButton/primaryButton';
import {ReactComponent as Image} from '../../img/porco2.svg';


const thirdSection = () => {
    return (
        <React.Fragment>
            <div className="thirdSection">
                <div className="thirdSectionInfo">
                    <p className="thirdSectionTitle">Já tem uma meta?</p>
                    <p>
                        Phasellus mollis eget enim sed ma
                        lesuada. Nulla facilisi. Aliquam nunc lacus, c
                        ommodo hendrerit
                        commodo pulvinar, suscipit eget ipsum. Nulla 
                        at sem ex.
                    </p>
                    <PrimaryButton buttonLink="/metas" buttonContent="VER METAS"/>
                    <p className="thirdSectionTitle">Não tem? Comece agora mesmo</p>

                </div>
                <div className="thirdSectionInfo">
                    <Image/>
                    <PrimaryButton buttonLink="/novameta" buttonContent="Nova Meta"/>
                </div>
                
            </div>
        </React.Fragment>
    )

}

export default thirdSection;