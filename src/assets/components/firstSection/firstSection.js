import React from 'react';
import './firstSection.css'
import PrimaryButton from '../primaryButton/primaryButton';
import {ReactComponent as Imagem}from '../../img/porco.svg'

const FirstSection = (props) => {
    return (
    <React.Fragment>
        <div className="firstSection">
            <div className="innerSection">
                <h1><b>{props.title}</b></h1>
                <p className="info">{props.info}</p>
                <PrimaryButton buttonContent="Nova Meta" buttonLink="/novameta"></PrimaryButton>
            </div>
            <Imagem className= "firstSectionImage"/>
        </div>
    </React.Fragment>
    )
}

export default FirstSection