import React, { useState } from 'react';
import BurgerMenu from '../burgerMenu/burgerMenu'
//import { render } from '@testing-library/react';


/* Modo Orientação Objetos 
class Header extends React.Component{
    render(){
        return (
            <React.Fragment>
                <h1>ALOU</h1>
            </React.Fragment> 
        )
    };
}
*/

/* Programação Funcional */

const Header = () => { 
    return(     
     <BurgerMenu/>              
    )
}

/* export {Header};
    ou
*/
export default Header;