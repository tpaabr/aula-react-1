import React from 'react';
import './itemMeta.css';

const itemMeta = (props) => {

    return (
        <React.Fragment>
            <il>
                <h2>{props.title}</h2>
                <p>{props.deadline}</p>
                <p>{props.amount}</p>
                <p>{props.desc}</p>
                <p>{props.saved}</p>
                <p>{props.categoryName}</p>
            </il>
            
        </React.Fragment>
    )
}



export default itemMeta;
