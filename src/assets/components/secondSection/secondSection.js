import React from 'react';
import './secondSection.css';
import {ReactComponent as Image1} from '../../img/Frame.svg';
import {ReactComponent as Image2} from '../../img/Vetor.svg';

const secondSection = () => {
    return (
    <React.Fragment>
        <div className="secondSection">
            <h2>Como Usar</h2>
            <div className="secondSectionInfo"> 
                <div className="item">
                    <Image1 className="secondSectionImage"/>
                    <p className="subTitle">Defina uma Meta</p>
                    <p className="desc">Pellentesque faucibus leo sed ar
                    cu vulputate rhoncus. Integer sit amet bland
                    it felis.</p>
                </div>
                <div className="item">
                    <Image2 className="secondSectionImage"/>
                    <p className="subTitle">Acompanhe o Progresso</p>
                    <p className="desc">Pellentesque faucibus 
                    leo sed arcu vulputate rhoncus. Intege
                    r sit amet blandit felis.</p>
                </div>
                <div className="item">
                    <Image1 className="secondSectionImage"/>
                    <p className="subTitle">Alcance seu objetivo</p>
                    <p className="desc">Pellentesque faucibus leo s
                    ed arcu vulputate rhoncus. Integer sit ame
                    t blandit felis.</p>
                </div>
            </div>
        </div>
    </React.Fragment>
    )
}

export default secondSection;