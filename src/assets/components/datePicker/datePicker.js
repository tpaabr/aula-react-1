import React from 'react';
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from 'react-datepicker'
import { useState } from 'react';

const InputDate = (props) => {

    const [startDate, setStartDate] = useState(new Date());

    return (
        <DatePicker 
            name={props.name} 
            selected= {startDate} 
            onChange={date => setStartDate(date)}

        />

    );
}

export default InputDate;