import React, { useState } from 'react';
import './footer.css';

const Footer = () => {
    return(
        <React.Fragment>
            <div className="backgroundFooter">
                <p className="textoFooter">Feito com amor</p>
            </div>
        </React.Fragment>
    )
}

export default Footer;