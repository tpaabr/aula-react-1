import React from 'react';
import { slide as Menu } from 'react-burger-menu';
import './burgerMenu.css';
import {BrowserRouter as Router, Link} from 'react-router-dom'

const BurgerMenu = () => {
    const showSettings = (event) => {
      event.preventDefault();
    }

    return (
      <Menu>
        <Link to="/" id="home" className="menu-item">Home</Link>
        <Link to="/novaMeta" id="novaMeta" className="menu-item" href="/novaMeta">Nova Meta</Link> 
        <Link to="/metas" id="metas" className="menu-item">Metas</Link>
        <a onClick={ showSettings } className="menu-item--small" href=""></a>
      </Menu>
    );
}
  
export default BurgerMenu