import axios from 'axios';
import React from 'react';

const API = axios.create({
    baseURL:"https://in-pig-bank.herokuapp.com/",
    responseType: "json"
});

export default API;

