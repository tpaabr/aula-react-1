import React from 'react';
import {BrowserRouter as Router, Link} from 'react-router-dom' 
import './primaryButton.css';

const PrimaryButton = (props) => {
    return(
        <Link to={props.buttonLink} className="primaryButtonStyle">
            {props.buttonContent}
        </Link>
    );
}

export default PrimaryButton;