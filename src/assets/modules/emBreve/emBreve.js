import React from 'react';
import {ReactComponent as Image} from '../../img/construcao.svg';
import './emBreve.css';
import PrimaryButton from '../../components/primaryButton/primaryButton';
import BurgerMenu from '../../components/burgerMenu/burgerMenu';

const EmBreve = () => {
    return (
        <React.Fragment>
            <BurgerMenu/>
            <div className="emBreveContent">
                <h2>Em Construção</h2>
                <Image/>
                <p className="emBreveText">Parece que essa página ainda não foi implementada... 
                    Tente novamente mais tarde!
                </p>
                <PrimaryButton buttonContent="Voltar" buttonLink="/"/>
            </div> 
        </React.Fragment>
    )
}

export default EmBreve;