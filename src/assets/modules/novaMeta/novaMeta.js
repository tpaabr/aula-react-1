import React from 'react';
import './novaMeta.css';
import { Formik } from 'formik';
import API from '../../components/api/api';
import InputDate from '../../components/datePicker/datePicker';
import BurgerMenu from '../../components/burgerMenu/burgerMenu'


const Basic = () => {


    return(
        
        <React.Fragment>
            <BurgerMenu/>
            <div>

                <h2 className="novaMetaTitle">Nova Meta</h2>
            </div>

                <Formik 
                    initialValues={{data:"", nomeCategoria:'7'}}
                    // validate={values => {
                    //     const errors = {};
                    //     if (!values.email) {
                    //         errors.email = 'Required';
                    //     } else if (
                    //         !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                    //     ) {
                    //         errors.email = 'Invalid email address';
                    //     }
                    //     return errors;
                    // }}
                    onSubmit={(values, { setSubmitting }) => {
                        setTimeout(() => {
                        alert(JSON.stringify(values, null, 2));
                        let sendMeta = {
                            title: values.titulo,
                            amount: parseFloat(values.quantia),
                            description: values.desc,
                            saved: 0,
                            category_id: parseInt(values.nomeCategoria)
                        }

                        API.post('/goals', sendMeta).then(console.log)
                        
                        }, 400);
                    }}
                    >
                    {({
                        values,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                        setFieldValue
                        /* and other goodies */
                    }) => (
                        <form className="formNovaMeta" onSubmit={handleSubmit}>
                            <div className="itemForm">
                                <label>Escolha data:</label>
                                <InputDate 
                                    type="text"
                                    name="data" 
                                    className="inputDate"
                                    value ={values.data}
                                    onBlur={handleBlur}
                                />
                                
                                {errors.data && touched.data && errors.data}
                            </div>
                            
                            <div className="itemForm">
                                <label>Título:</label>
                                <input className="itemInput"
                                    placeholder="Nome da Meta"
                                    type="text"
                                    name="titulo"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.titulo}
                                />
                                {errors.titulo && touched.titulo && errors.titulo}
                            </div>

                            

                            <div className="itemForm">
                                <label>Quantia:</label>
                                <input className="itemInput"
                                    placeholder="Quantia em $ a economizar"
                                    type="text"
                                    name="quantia"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.quantia}
                                />
                                {errors.quantia && touched.quantia && errors.quantia}
                            </div>

                            <div className="itemForm">
                                <label>Categoria:</label>
                                <div className="doubleInput">
                                    <select className="itemInput"
                                        name="nomeCategoria"
                                        onChange={handleChange}
                                        value={values.nomeCategoria}
                                    > 
                                        <option value="7">Estudos</option>
                                        <option value="8">Viagem</option>
                                        <option value="9">Carro</option>
                                        
                                    </select>
                                    <input className="pickCollor"
                                        placeholder="Cor desejada(#)"
                                        type="text"
                                        name="cor"
                                        
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.cor}
                                    />
                                </div>
                                {errors.nomeCategoria && touched.nomeCategoria && errors.nomeCategoria}
                                {errors.cor && touched.cor && errors.cor}
                            </div>

                            <div className="itemForm">
                                <label>Descrição:</label>
                                <input className="descInput"
                                    placeholder="Uma frase para auto-motivação"
                                    type="textArea"
                                    name="desc"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.desc}
                                />
                                {errors.desc && touched.desc && errors.desc}
                            </div>

                        <button  className="submitButton"type="submit" disabled={isSubmitting}>
                            Submit
                        </button>
                        </form>
                    )}
                </Formik>
                        
        </React.Fragment>
    )
}

  
export default Basic;
