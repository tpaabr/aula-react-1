import React from 'react';
import '../../App.css';
import FirstSection from '../components/firstSection/firstSection';
import SecondSection from '../components/secondSection/secondSection';
import ThirdSection from '../components/thirdSection/thirdSection';
import BurgerMenu from '../components/burgerMenu/burgerMenu';


const Home = () => {
    return(
    <React.Fragment>
      <BurgerMenu/>
      <FirstSection title="Pig Bank" info="Lorem ipsum dolor sit amet, c
      onsectetur adipiscing elit. Donec a leo eu enim auctor fauc
      ibus id eget dolor. P
      ellentesque faucibus leo sed arcu vulputate rhoncus."/>
      <SecondSection/>
      <ThirdSection/>      
    </React.Fragment>
    )
}

export default Home;