import React from 'react';
import './notFound.css';
import {ReactComponent as Image} from '../../img/confuso.svg'
import PrimaryButton from '../../components/primaryButton/primaryButton';
import BurgerMenu from '../../components/burgerMenu/burgerMenu'

const NotFound = () => {
    return (
        <React.Fragment>
            <BurgerMenu/>
            <div className="notFoundContent">
                <h2 className="notFoundTitle">Erro 404</h2>
                <Image/>
                <p className="notFoundText"> Ops! Parece que o endereço que você digitou está incorreto...</p>
                <PrimaryButton   buttonContent=" Voltar " buttonLink="/" />
            </div>

        </React.Fragment>
    )
}

export default NotFound