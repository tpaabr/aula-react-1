import React from 'react';
import './App.css';
import Home from './assets/modules/home';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Layout from './assets/components/layout/layout';
import NovaMeta from './assets/modules/novaMeta/novaMeta';
import NotFound from './assets/modules/notFound/notFound';
import EmBreve from './assets/modules/emBreve/emBreve';
import Metas from './assets/modules/metas/metas'


function App() {
  return (
    <Layout>
      <Router>
        <Switch>
          <Route exact path="/"><Home/></Route>
          <Route exact path="/novameta"><NovaMeta/></Route>
          <Route exact path="/metas"><Metas/></Route>
          <Route exact path="*"><NotFound/></Route>
        </Switch>
      </Router>
    </Layout>
  );
}

export default App;
